package fr.afpa.cda.metier.entity;


public class Authentification {
	private String login;
	private String mdp;
	private boolean admin;
	private Personne pers;
	
	
	/**
	 * @param login
	 * @param mdp
	 * @param admin
	 */
	public Authentification(String login, String mdp, boolean admin) {
		super();
		this.login = login;
		this.mdp = mdp;
		this.admin = admin;
	}

	
	public Authentification() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the login
	 */
	public String getLogin() {
		return login;
	}
	/**
	 * @param login the login to set
	 */
	public void setLogin(String login) {
		this.login = login;
	}
	/**
	 * @return the mdp
	 */
	public String getMdp() {
		return mdp;
	}
	/**
	 * @param mdp the mdp to set
	 */
	public void setMdp(String mdp) {
		this.mdp = mdp;
	}
	/**
	 * @return the admin
	 */
	public boolean isAdmin() {
		return admin;
	}
	/**
	 * @param admin the admin to set
	 */
	public void setAdmin(boolean admin) {
		this.admin = admin;
	}

	public Personne getPers() {
		return pers;
	}

	public void setPers(Personne pers) {
		this.pers = pers;
	}


	@Override
	public String toString() {
		return "Authentification [login=" + login + ", mdp=" + mdp + ", admin=" + admin + ", pers=" + pers + "]";
	}
	
	
	
	
}
