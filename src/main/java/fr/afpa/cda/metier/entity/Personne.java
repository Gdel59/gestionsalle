package fr.afpa.cda.metier.entity;

import java.sql.Date;

public class Personne {
	private String nom;
	private String prenom;
	private Date dateNais;
	private int numAdr;
	private String nomAdr;
	private String ville;
	private String mail;
	private String tel;
	/**
	 * @param nom
	 * @param prenom
	 * @param dateNais
	 * @param numAdr
	 * @param nomAdr
	 * @param ville
	 * @param mail
	 * @param tel
	 */
	public Personne( String nom, String prenom, Date dateNais, int numAdr, String nomAdr, String ville,
			String mail, String tel) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.dateNais = dateNais;
		this.numAdr = numAdr;
		this.nomAdr = nomAdr;
		this.ville = ville;
		this.mail = mail;
		this.tel = tel;
	}
	public Personne() {
		// TODO Auto-generated constructor stub
	}
	/**
	 * @return the nom
	 */
	public String getNom() {
		return nom;
	}
	/**
	 * @param nom the nom to set
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}
	/**
	 * @return the prenom
	 */
	public String getPrenom() {
		return prenom;
	}
	/**
	 * @param prenom the prenom to set
	 */
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	/**
	 * @return the dateNais
	 */
	public Date getDateNais() {
		return dateNais;
	}
	/**
	 * @param dateNais the dateNais to set
	 */
	public void setDateNais(Date dateNais) {
		this.dateNais = dateNais;
	}
	/**
	 * @return the numAdr
	 */
	public int getNumAdr() {
		return numAdr;
	}
	/**
	 * @param numAdr the numAdr to set
	 */
	public void setNumAdr(int numAdr) {
		this.numAdr = numAdr;
	}
	/**
	 * @return the nomAdr
	 */
	public String getNomAdr() {
		return nomAdr;
	}
	/**
	 * @param nomAdr the nomAdr to set
	 */
	public void setNomAdr(String nomAdr) {
		this.nomAdr = nomAdr;
	}
	/**
	 * @return the ville
	 */
	public String getVille() {
		return ville;
	}
	/**
	 * @param ville the ville to set
	 */
	public void setVille(String ville) {
		this.ville = ville;
	}
	/**
	 * @return the mail
	 */
	public String getMail() {
		return mail;
	}
	/**
	 * @param mail the mail to set
	 */
	public void setMail(String mail) {
		this.mail = mail;
	}
	/**
	 * @return the tel
	 */
	public String getTel() {
		return tel;
	}
	/**
	 * @param tel the tel to set
	 */
	public void setTel(String tel) {
		this.tel = tel;
	}
	@Override
	public String toString() {
		return "Personne [nom=" + nom + ", prenom=" + prenom + ", dateNais=" + dateNais + ", numAdr=" + numAdr
				+ ", nomAdr=" + nomAdr + ", ville=" + ville + ", mail=" + mail + ", tel=" + tel + "]";
	}
	
	
}
