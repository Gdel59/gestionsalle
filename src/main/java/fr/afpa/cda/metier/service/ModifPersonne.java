package fr.afpa.cda.metier.service;

import java.util.ArrayList;

import fr.afpa.cda.dto.service.ModifPersonneDto;
import fr.afpa.cda.metier.entity.Authentification;
import fr.afpa.cda.metier.entity.Personne;

public class ModifPersonne {
	public boolean CreerUser(Personne p, Authentification auth) {
		ModifPersonneDto spd = new ModifPersonneDto();
		return spd.CreerUser(p, auth);
	}

	public boolean EnregistrerAu(String login, String mdp) {
		ModifPersonneDto mpdt = new ModifPersonneDto();
		Authentification auth = mpdt.EnregistrerAuthentif(login, mdp);
		if (auth == null) {
			return false;
		}
		return true;

	}

	public void DeleteUser(String login, String mdp) {
		ModifPersonneDto sdp = new ModifPersonneDto();
		sdp.DeleteUti(login, mdp);
	}

	public ArrayList<Authentification> listerAuthentification() {
		ModifPersonneDto mpdt = new ModifPersonneDto();
		ArrayList<Authentification> liste = mpdt.listerAuthentificationDto();
		System.out.println(liste);
		return liste;

	}

	public void ModifierPersonne(Personne p, Authentification auth) {
		ModifPersonneDto mpd = new ModifPersonneDto();
		mpd.ModifierUser(p, auth);

	}

	
	
	public Authentification recupInfosAuth(String login, String mdp) {
		ModifPersonneDto mpdt = new ModifPersonneDto();
		Authentification auth = mpdt.EnregistrerAuthentif(login, mdp);
		
		return auth;

	}
}
