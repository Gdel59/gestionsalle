package fr.afpa.cda.dao.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import fr.afpa.cda.dao.entity.PersonneDao;


@Entity
@Table(name = "authentification")
public class AuthentificationDao {
	@Column
	@Id
	private String login;
	@Column(nullable = false)
	private String mdp;
	@Column(nullable = false)
	private boolean admin;
	@OneToOne(cascade = CascadeType.ALL, orphanRemoval = true )
	@OnDelete( action = OnDeleteAction.CASCADE )
	@JoinColumn(name="idPers") 
	
	private PersonneDao perso;
	
	public AuthentificationDao() {
		super();
	}
	/**
	 * @param login
	 * @param mdp
	 * @param admin
	 */
	public AuthentificationDao(String login, String mdp, boolean admin) {
		super();
		this.login = login;
		this.mdp = mdp;
		this.admin = admin;
	}
	/**
	 * @return the login
	 */
	public String getLogin() {
		return login;
	}
	/**
	 * @param login the login to set
	 */
	public void setLogin(String login) {
		this.login = login;
	}
	/**
	 * @return the mdp
	 */
	public String getMdp() {
		return mdp;
	}
	/**
	 * @param mdp the mdp to set
	 */
	public void setMdp(String mdp) {
		this.mdp = mdp;
	}
	/**
	 * @return the admin
	 */
	public boolean isAdmin() {
		return admin;
	}
	/**
	 * @param admin the admin to set
	 */
	public void setAdmin(boolean admin) {
		this.admin = admin;
	}
	/**
	 * @return the perso
	 */
	public PersonneDao getPerso() {
		return perso;
	}
	/**
	 * @param perso the perso to set
	 */
	public void setPerso(PersonneDao perso) {
		this.perso = perso;
	}
	
	
	
}
