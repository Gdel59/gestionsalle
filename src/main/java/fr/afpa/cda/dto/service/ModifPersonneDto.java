package fr.afpa.cda.dto.service;

import java.util.ArrayList;

import fr.afpa.cda.dao.entity.AuthentificationDao;
import fr.afpa.cda.dao.entity.PersonneDao;
import fr.afpa.cda.dao.service.ModifPersonneDao;
import fr.afpa.cda.metier.entity.Authentification;
import fr.afpa.cda.metier.entity.Personne;

public class ModifPersonneDto {

	public boolean CreerUser(Personne p, Authentification auth) {
		ModifPersonneDao mp = new ModifPersonneDao();
		PersonneDao perDao = new PersonneDao();
		AuthentificationDao authDao = new AuthentificationDao();
		authDao.setLogin(auth.getLogin());
		authDao.setMdp(auth.getMdp());
		authDao.setAdmin(auth.isAdmin());
		perDao.setNom(p.getNom());
		perDao.setPrenom(p.getPrenom());
		perDao.setDateNais(p.getDateNais());
		perDao.setNumAdr(p.getNumAdr());
		perDao.setNomAdr(p.getNomAdr());
		perDao.setVille(p.getVille());
		perDao.setMail(p.getMail());
		perDao.setTel(p.getTel());
		authDao.setPerso(perDao);
		perDao.setAuth(authDao);
		return mp.CreerUser(perDao, authDao);
	}

	public Authentification EnregistrerAuthentif(String login, String mdp) {
		ModifPersonneDao mpd = new ModifPersonneDao();
		AuthentificationDao authDao = mpd.Enregistrement(login, mdp);
		Personne personne = new Personne();
		Authentification auth = new Authentification();
		if (authDao != null) {
			auth.setLogin(authDao.getLogin());
			auth.setMdp(authDao.getMdp());
			auth.setAdmin(authDao.isAdmin());
			personne.setNom(authDao.getPerso().getNom());
			personne.setPrenom(authDao.getPerso().getPrenom());
			personne.setDateNais(authDao.getPerso().getDateNais());
			personne.setMail(authDao.getPerso().getMail());
			personne.setNumAdr(authDao.getPerso().getNumAdr());
			personne.setNomAdr(authDao.getPerso().getNomAdr());
			personne.setVille(authDao.getPerso().getVille());
			personne.setTel(authDao.getPerso().getTel());
			auth.setPers(personne);
			return auth;
		}
		return null;

	}

	
	public ArrayList<Authentification> listerAuthentificationDto(){
		ModifPersonneDao mpd = new ModifPersonneDao();
		ArrayList<AuthentificationDao> listeDao = mpd.listerAuthentificationDao();
		ArrayList<Authentification> liste = new ArrayList<Authentification>();

		for (AuthentificationDao authDao : listeDao) {
			Personne pers = new Personne();
			Authentification auth = new Authentification();
			pers.setNom(authDao.getPerso().getNom());
			pers.setPrenom(authDao.getPerso().getPrenom());
			pers.setDateNais(authDao.getPerso().getDateNais());
			pers.setMail(authDao.getPerso().getMail());
			pers.setTel(authDao.getPerso().getTel());
			pers.setNumAdr(authDao.getPerso().getNumAdr());
			pers.setNomAdr(authDao.getPerso().getNomAdr());
			pers.setVille(authDao.getPerso().getVille());
			
			auth.setLogin(authDao.getLogin());
			auth.setMdp(authDao.getMdp());
			auth.setAdmin(authDao.isAdmin());
			auth.setPers(pers);
			liste.add(auth);
		}
		System.out.println(liste);
		return liste;
	}
	public void ModifierUser (Personne p, Authentification auth) {
		ModifPersonneDao mp = new ModifPersonneDao();
		PersonneDao perDao = new PersonneDao();
		AuthentificationDao authDao = new AuthentificationDao();
		authDao.setLogin(auth.getLogin());
		authDao.setMdp(auth.getMdp());
		authDao.setAdmin(auth.isAdmin());
		perDao.setNom(p.getNom());
		perDao.setPrenom(p.getPrenom());
		perDao.setDateNais(p.getDateNais());
		perDao.setNumAdr(p.getNumAdr());
		perDao.setNomAdr(p.getNomAdr());
		perDao.setVille(p.getVille());
		perDao.setMail(p.getMail());
		perDao.setTel(p.getTel());
		authDao.setPerso(perDao);
		perDao.setAuth(authDao);
		mp.ModifierUser(perDao, authDao);
	}

	public Authentification DeleteUti(String login, String mdp) {
		ModifPersonneDao mpd = new ModifPersonneDao();
		AuthentificationDao authDao = mpd.DeleteUtili(login, mdp);
		Authentification auth = new Authentification();
		if (authDao != null) {
			auth.setLogin(authDao.getLogin());
			auth.setMdp(authDao.getMdp());
			auth.setAdmin(authDao.isAdmin());
			return auth;
		}
		return null;
	}
	
}

