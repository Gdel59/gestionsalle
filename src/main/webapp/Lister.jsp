<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link rel="stylesheet" href="style/bootstrap.min.css" type="text/css">
<link rel="stylesheet" href="style/style.css" type="text/css">
</head>
<body>
	<div class="container-fluid titre">
		<h1>Liste des utilisateurs</h1>
	</div>
	<div class="container-fluid">
		<table>
			<tr>

				<th>Login</th>
				<th>Admin</th>
				<th>Prenom</th>
				<th>Nom</th>
				<th>Date de naissance</th>
				<th>E-mail</th>
				<th>T�l�phone</th>
				<th>Num�ro rue</th>
				<th>Nom rue</th>
				<th>Ville</th>



			</tr>
			<c:forEach items="${ liste }" var="listePersonne">
				<tr>

					<td><c:out value="${ listePersonne['login'] }" /></td>
					<td><c:out value="${ listePersonne['admin'] }" /></td>
					<td><c:out value="${ listePersonne['pers'].prenom }" /></td>
					<td><c:out value="${ listePersonne.pers.nom }" /></td>
					<td><c:out value="${ listePersonne.pers.dateNais }" /></td>
					<td><c:out value="${ listePersonne.pers.mail }" /></td>
					<td><c:out value="${ listePersonne.pers.tel }" /></td>
					<td><c:out value="${ listePersonne.pers.numAdr }" /></td>
					<td><c:out value="${ listePersonne.pers.nomAdr }" /></td>
					<td><c:out value="${ listePersonne.pers.ville }" /></td>
					<td>
					<c:url value="/ModifierUser" var="LienModif">
					<c:param name="login" value="${ listePersonne['login'] }" />
					<c:param name="password" value="${ listePersonne['mdp'] }" />
					</c:url>
					<a href="${LienModif}"><button class="btntable">Modifier</button>
					</a>	
					</td>
					<c:url value="/Delete" var="LienSuppr">
					<c:param name="login" value="${ listePersonne['login'] }" />
					<c:param name="mdp" value="${ listePersonne['mdp'] }" />
					</c:url>
					<td><a href="${ LienSuppr }"><button class="btntable2">Supprimer</button></a></td>
					
					




				</tr>
			</c:forEach>
		</table>
	</div>

</body>
</html>