<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Sans_titre01.mp5</title>
<link rel="stylesheet" href="style/bootstrap.min.css" type="text/css">
<link rel="stylesheet" href="style/style.css" type="text/css">
</head>
<body>
	<div class="container-fluid titre">
		<h1>Cr�ation Personne</h1>
	</div>
	<div class="container formulaire">
		<form method="post" action="Creation">
			<div class="row"> 
				<div class="col-md-6">Login</div>
  				<div class="col-md-6">Mot de passe</div>
			</div>
			<div class="row"> 
				<div class="col-md-6"><input type="text" name="login" ></div>
  				<div class="col-md-6"><input type="password" name="password" ></div>
			</div>
			<div class="row"> 
				<div class="col-md-6">Nom</div>
  				<div class="col-md-6">Pr�nom</div>
			</div>
			<div class="row"> 
				<div class="col-md-6"><input type="text" name="nom" ></div>
  				<div class="col-md-6"><input type="text" name="prenom" "></div>
			</div>
			<div class="row"> 
				<div class="col-md-6">Date de Naissance</div>
  				<div class="col-md-6">Adresse Mail</div>
			</div>
			<div class="row"> 
				<div class="col-md-6"><input type="date" name="dateNais" min="1900-01-02"></div>
  				<div class="col-md-6"><input type="email" name="mail"></div>
			</div>
			<div class="row"> 
				<div class="col-md-6">N� de rue</div>
  				<div class="col-md-6">Rue</div>
			</div>
			<div class="row"> 
				<div class="col-md-6"><input type="text" name="numRue" pattern="[0-9]{1,5}"></div>
  				<div class="col-md-6"><input type="text" name="rue" ></div>
			</div>
			<div class="row"> 
				<div class="col-md-6">Ville</div>
  				<div class="col-md-1">Tel</div>
			</div>
			<div class="row"> 
				<div class="col-md-6"><input type="text" name="ville"></div>
  				<div class="col-md-1"><input type="text" name="tel" pattern="[0-9]{10}"></div>
			</div>	
			<div class="row"> 
				<div class="col-md-6">Admin  <input type="checkbox" name="admin" value="true"></div>
			</div>	
			<br>
			<input type="submit" value="Soumettre">
		</form>
	</div>
</body>
</html>