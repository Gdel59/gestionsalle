<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Sans_titre01.mp5</title>
<link rel="stylesheet" href="style/bootstrap.min.css" type="text/css">
<link rel="stylesheet" href="style/style.css" type="text/css">
</head>
<body>
	<div class="container-fluid titre">
		<h1>Modifier un utilisateur </h1>
	</div>
	<div class="container formulaire">
		<form method="post" action="Modification">
			<div class="row"> 
				<div class="col-md-6">Login</div>
  				<div class="col-md-6">Mot de passe</div>
			</div>
			<div class="row"> 
				<div class="col-md-6"><input type="text" name="login" value="${ auth.login }"></div>
  				<div class="col-md-6"><input type="password" name="password" value="${ auth.mdp }"></div>
			</div>
			<div class="row"> 
				<div class="col-md-6">Nom</div>
  				<div class="col-md-6">Pr�nom</div>
			</div>
			<div class="row"> 
				<div class="col-md-6"><input type="text" name="nom" value="${ auth.pers.nom }"></div>
  				<div class="col-md-6"><input type="text" name="prenom" value="${ auth.pers.prenom }"></div>
			</div>
			<div class="row"> 
				<div class="col-md-6">Date de Naissance</div>
  				<div class="col-md-6">Adresse Mail</div>
			</div>
			<div class="row"> 
				<div class="col-md-6"><input type="date" name="dateNais" min="1900-01-02" value="${ auth.pers.dateNais }"></div>
  				<div class="col-md-6"><input type="email" name="mail"value="${auth.pers.mail}"></div>
			</div>
			<div class="row"> 
				<div class="col-md-6">Num�ro de rue</div>
  				<div class="col-md-6">Rue</div>
			</div>
			<div class="row"> 
				<div class="col-md-6"><input type="text" name="numRue" pattern="[0-9]{1,5}" value="${ auth.pers.numAdr }"></div>
  				<div class="col-md-6"><input type="text" name="rue"value="${ auth.pers.nomAdr }"></div>
			</div>
			<div class="row"> 
				<div class="col-md-6">Ville</div>
  				<div class="col-md-1">Tel</div>
			</div>
			<div class="row"> 
				<div class="col-md-6"><input type="text" name="ville" value="${ auth.pers.ville }"></div>
  				<div class="col-md-1"><input type="text" name="tel" pattern="[0-9]{10}"value="${ auth.pers.tel }"></div>
			</div>	
			<div class="row"> 
				<div class="col-md-6">Admin  <input type="checkbox" name="admin" value=""></div>
			</div>	
			<input type="submit" value="Modifier">
		</form>
	</div>
</body>
</html>